<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	$deportesQuery = "SELECT * FROM categorias";
	$deportesArray = $db->query($deportesQuery);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Deportes Nicaragua</title>

    <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/categorias.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/modal.css" rel="stylesheet">
</head>

<body>
    <!-- Content of the page, events, results, etc... -->
	<div class="col-md-12" style="margin-top: 100px;">
		<div class="form-group">
			<div class="col-md-6">
			<label for="verDep">Seleccionar deporte</label>
			<select id="verDep" class="form-control">
				<option></option>
				<?php
					$depQuery = "SELECT * FROM categorias";
					$depArray = $db->query($depQuery);
				?>
				<? while($dep = mysqli_fetch_assoc($depArray)) : ?>
					<option value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
				<? endwhile; ?>
			</select>
			</div>
			<div class="col-md-6">
			<label for="verLig">Seleccionar liga</label>
			<select id="verLig" class="form-control">
				<option></option>
			</select>
			</div>
		</div>
		<div style="margin-top: 100px;">
			<h2>Eventos</h2>
			<table class="table table-responsive  table-bordered" style="border-color: black;">
				<thead>
					<th class="text-center">Equipos</th>
					<th class="text-center">Fecha</th>
					<th class="text-center">Hora</th>
					<th class="text-center">Ver detalles</th>
				</thead>

				<tbody id="infoEv">
				</tbody>
			</table>
		</div>
	</div>
</body>
	<!-- jQuery -->
	<script src="../js/jquery.min.js"></script>    
    <!-- Bootstrap Core JavaScript -->
    <script src="../css/bootstrap/js/bootstrap.min.js"></script>
    <!--Custom helpers functions-->
    <script src="../helpers/helpers.js"></script>
    <script type="text/javascript">

	function detailsModal(id)
	{
		var data = {'id' : id};
		$.ajax({
			url: '/deportesNic/helpers/detailsModal.php',
			method : 'post',
			data : data,
			
			success : function(data) {
				console.log(data);
				$('body').append(data);
				$('#detailsModal').modal('toggle');
			},

			error : function() {
				alert('No se pueden cargar los detalles :(');
			}
		});
	}

    	$('select[id="verDep"]').change(function () {
		var ID = $('#verDep').val();
		$.ajax({
		url : '/deportesNic/helpers/getLigas.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#verLig').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
		});
	});

	$('select[id="verLig"]').change(function () {
		var ID = $('#verLig').val();
		$.ajax({
		url : '/deportesNic/helpers/getEventosTableUser.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#infoEv').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
		});
	});


    </script>

</html>