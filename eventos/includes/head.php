<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Deportes Nicaragua</title>

    <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/categorias.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/modal.css" rel="stylesheet">
</head>