<?php
require_once 'includes/head.php';
?>
<body id="page-top" class="index">
<div id="skipnav"><a href="#maincontent">Skip to main content</a></div>
<?php
  require_once 'includes/navigation.php';
  require_once 'includes/header.php';
  require_once 'includes/categorias.php';
  require_once 'includes/acerca.php';
  require_once 'includes/contacto.php';
  require_once 'includes/footer.php';
?>