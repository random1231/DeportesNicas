
    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Hans Blanco
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php
    require_once 'includes/jsIncludes.php';
?>

</body>

</html>