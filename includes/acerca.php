<!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Acerca de...</h2>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2 text-justify">
                    <p>Este sitio se desarollo para mantener al publico informado acerca de los deportes en Nicaragua, desde proximos eventos, eventos pasados, resultados, tabla de posiciones, etc </p>
                </div>
                <div class="col-lg-4 text-center">
                    <p>Desarrollado por estudiantes de la carrera ingenieria en computacion de la Universidad Nacional de Ingenieria:</p>
                      Hans Blanco -  
                      Jorge Mendoza<br>
                      Elena Martinez-
                      Ronnie Rivera<br>
                      Emily Sinclair
                </div>
            </div>
        </div>
    </section>