<!-- Plugin JavaScript -->
    <script src="js/jquery-easing.js"></script>
    <script src="js/freelancer.min.js"></script>
    <script type="text/javascript">
    	if($(window).width() <= 525)
    	{
    		$(".portfolio-item").removeClass("col-xs-6");
    		$(".portfolio-item").addClass("col-xs-12");
    	}

    	$(window).resize(function(){
	    	if($(window).width() <= 525)
	    	{
	    		$(".portfolio-item").removeClass("col-xs-6");
	    	}

	    	else
	    	{
	    		$(".portfolio-item").removeClass("col-xs-12");
	    		$(".portfolio-item").addClass("col-xs-6");
	    	}

	    });
    </script>