 <!-- Header -->
    <header>
        <div class="container" id="maincontent" tabindex="-1">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/cover.png"  style="max-width: 100px; max-height: 100px" alt="">
                    <div class="intro-text">
                        <h1 class="name">Deportes Nica</h1>
                        <br>
                        <span class="skills">¡Lo último en el deporte Nica!</span>
                    </div>
                </div>
            </div>
        </div>
    </header>