<?php
    require_once 'core/init.php';
    $categoriesQuery = "SELECT * FROM categorias WHERE incluida=1";
    $categoriesArray = $db->query($categoriesQuery);
?>

<section >
        <div id="portfolio" class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Categorias</h2>
                </div>
            </div>
              <br>
            <div class="row">
                <? while($cat = mysqli_fetch_assoc($categoriesArray)) : ?>
                <div class="col-xs-6 col-sm-4 portfolio-item">
                    <a href="categorias?dep=<?= $cat['id']; ?>" class="portfolio-link">
                        <div class="caption">
                            <div class="caption-content">
                              <strong><?=  $cat['nombre']; ?></strong>

                            </div>
                        </div>
                        <img src="<?= $cat['imagen']; ?>" class="img-responsive center-block cateImg" alt="Cabin">
                    </a>
                </div>
                <? endwhile; ?>
            </div>
        </div>
        <br>
        <button class="btn btn-success center-block">Ver todas las categorias</button>
    </section>