<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	if(isset($_POST['ID']))
		$ID = (int)$_POST['ID'];
	$finalizado = (int)$_POST['finalizado'];

?>
<div class="col-md-12" style="margin-top: 120px; color: black">
	<div class="form-group">
		<div class="col-md-6">
		<label for="verLig">Seleccionar liga</label>
		<select id="verLig" class="form-control">
			<option></option>
		</select>
		</div>
	</div>
	<div style="margin-top: 100px;">
		<h2><?= (($finalizado == 1) ? 'Eventos pasados' : 'Proximos eventos') ?></h2>
		<table class="table table-responsive table-striped table-bordered" style="border-color: black;">
			<thead>
				<th class="text-center">Equipos</th>
				<th class="text-center">Fecha</th>
				<th class="text-center">Hora</th>
				<th class="text-center">Ver detalles</th>
			</thead>

			<tbody id="infoEv">
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	
        function ligaInfo()
        {
	        var ID = <?= $ID ?>;;
	        $.ajax({
	        url : '/deportesNic/helpers/getLigas.php',
	        type : 'POST',
	        data : {ID : ID},
	        success : function(htmlData, x){
	        	console.log(htmlData);
	            $('#verLig').html(htmlData);
	        },
	        error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
	        });
	    }
        ligaInfo();

        $('select[id="verLig"]').change(function () {
		var ID = $('#verLig').val();
		var finalizado = <?= $finalizado ?>;
		$.ajax({
		url : '/deportesNic/helpers/getEventosTableUser.php',
		type : 'POST',
		data : {ID : ID, finalizado : finalizado},
		success : function(htmlData, x){
			console.log(htmlData);
			$('#infoEv').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
		});
	});
</script>