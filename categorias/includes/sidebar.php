<div id="sidebar-wrapper" >
    <ul class="sidebar-nav">
        <div class="panel-group" style="width: 179px;">
            <div class="panel panel-default">
                <div class="panel-heading otrDep" id="otros" style="background-color: #2C3E50; ; color: white;">
                    <center><a data-toggle="collapse" class="panel-title" href="#collapse">
                    <?= $selDep; ?>
                    </a></center>
                </div>
                <div id="collapse" class="panel-collapse collapse" style="background-color: #2C3E50; border-color: #2C3E50; color: white;">
                    <ul class="list-group" >
                        <? while($dep = mysqli_fetch_assoc($catResult)) :?>
                            <li class="list-group-time"><a href="?dep=<?= $dep['id']; ?>"><?= $dep['nombre']; ?></a></li>
                        <? endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <li class="sidebar-brand" style="margin-bottom: 20px;">
            <h3><a><?= $cateArray['nombre']; ?></a></h3>
        </li>
        <li style="margin-bottom: 3px;">
            <a href="index.php?dep=<?= $cateArray['id']; ?>">Inicio</a>
        </li>
        <li style="margin-bottom: 3px;">
            <?php
                if (isset($_GET['dep']) && !empty($_GET['dep']))
                    $dep = (int)$_GET['dep'];
            ?>
            <a href="#" onclick="proximosEventos(<?= $dep ?>);">Proximos eventos</a>
        </li>
        <li>
            <a href="#" onclick="eventosPasados(<?= $dep ?>);">Eventos pasados</a>
        </li>
    </ul>
</div>