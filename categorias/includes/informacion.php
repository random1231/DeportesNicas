<?php      
    ob_start();
?>

 <!-- Header -->
    <header>
        <div class="container" id="informacion" tabindex="-1">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="<?= ((!empty($cateArray['imagen']) ? $cateArray['imagen'] : '')); ?>"  style="max-width: 100px; max-height: 100px" alt="">
                    <div class="intro-text" style="margin-left: 20px;">
                        <h1 class="name"> <?= ((!empty($cateArray) ? $cateArray['nombre'] : 'Deportes Nicaragua'));  ?> </h1>
                        <br>
                        <span class="skills"><?= ((!empty($cateArray) ? $cateArray['descripcion'] : 'Selecciona un deporte en el menu')); ?></span>
                        <br>
                        <br>
                        <? if($selDep != "Escoge deportes"): ?>
                            <!--<a class="btn btn-primary">Deportistas destacados</a>-->
                        <?endif;?>
                        
                    </div>
                </div>
            </div>
        </div>
    </header>
<?= ob_get_clean(); ?>