<?php
    require_once 'includes/head.php';
    require_once 'includes/navigation.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/config.php';
    require_once BASEURL.'core/init.php';

    $catQuery = "SELECT * FROM categorias WHERE incluida=1";
    $catResult = $db->query($catQuery);
    $cateArray=null;
    $selDep = '';

    if(isset($_GET) && !empty($_GET))
    {
        require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
        $cateID = (int)$_GET['dep'];
        $cateQuery = $db->query("SELECT * FROM categorias WHERE id='$cateID'");
        $cateArray =  mysqli_fetch_assoc($cateQuery);
        $selDep = "Otros deportes";
    }

    if(empty($cateArray))
        $selDep = "Escoge deportes";
?>
<body>

    <div id="wrapper" class="toggled">
        <!-- Sidebar -->
        <? require_once 'includes/sidebar.php';?>
        <!-- Content of the page, events, results, etc... -->
        <div id="page-content-wrapper" >
            <?php require_once 'includes/informacion.php'; ?>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../css/bootstrap/js/bootstrap.min.js"></script>
    <script src="../helpers/helpers.js"></script>
    <script>
        $(".menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        function proximosEventos(ID)
        {
            $('#informacion').remove();
            $.ajax({
                url : '/deportesNic/categorias/includes/eventos.php',
                type : 'POST',
                data : {ID : ID, finalizado : 0},
                success : function(htmlData, x){
                    $('header').html(htmlData);
                },
                error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
                });
        }

        function eventosPasados(ID)
        {
            $('#informacion').remove();
            $.ajax({
                url : '/deportesNic/categorias/includes/eventos.php',
                type : 'POST',
                data : {ID : ID, finalizado : 1},
                success : function(htmlData, x){
                    $('header').html(htmlData);
                },
                error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
                });
        }
    </script>

</body>

</html>
