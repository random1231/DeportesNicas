<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/helpers/helpers.php';
    require_once 'includes/head.php';

    $email = '';
    if(isset($_POST['email']))
    {
        $email = sanitize($_POST['email']);
        $email = trim($email);
    }

    $password = '';
    if(isset($_POST['password']))
    {
        $password = sanitize($_POST['password']);
        $password = trim($password);
    }

    $errors = '';
    $displayError = '';

    if(isset($_POST) && !empty($_POST))
    {
        //Form validation
     /*   //password > 6
        if(strlen($password) < 6)
        {
            $errors[] = 'Enter a password with at least 6 characters';
        }*/

        $userQuery = "SELECT * FROM usuarios WHERE email='$email'";
        $userResult = $db->query($userQuery);
        $userResultArray = mysqli_fetch_assoc($userResult);

        //Check if user exist in the database
        $userCount = mysqli_num_rows($userResult);
        if($userCount < 1)
        {
            $errors = 'Ese email no existe en nuestra base de datos';
        }

        //$hashed = password_hash($password, PASSWORD_DEFAULT);

        elseif(!password_verify($password, $userResultArray['contrasena']))
        {
            $errors = "Contraseña incorrecta";
        }

        //validate email
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $errors = 'Ingresa un email valido';
        }

        if(empty($_POST['email']) || empty($_POST['password']))
        {
            $errors = 'Debes ingresar tu email y contraseña';
        }
        
        //Check errors
        if($errors != '')
        {
            $displayError = '<p class="text-danger">'.$errors.'</p>';
        }

        else
        {
            $userID = $userResultArray['id'];
            login($userID);
        }
    }
?>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <?= $displayError ?>
                <h2>Login</h2>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <form name="sentMessage" id="contactForm" method="post" novalidate="">
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="email">Email</label>
                            <input type="email"  name="email" class="form-control" placeholder="Email" value="<?= ( (isset($_POST['email'])) ? $_POST['email'] : '') ?>"  id="email" required data-validation-required-message="Ingresa tu correo">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="pass">Contraseña</label>
                            <input type="password" name="password" class="form-control" placeholder="Contraseña" id="pass" required data-validation-required-message="Ingresa tu contraseña">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <a href="index.php" class="btn btn-success btn-lg">Cancelar</a>
                        </div>
                        <div class="form-group col-xs-6">
                            <button type="submit" class="btn btn-success btn-lg pull-right">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php
    require_once 'includes/jsIncludes.php'
?>