<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';

	if(!isLoggedIn())
	{
		loginErrorRedirect();
	}
	
	include 'includes/head.php';
	include 'includes/navigation.php';

	$equiposQuery = "SELECT * FROM equipo";
	$equiposArray = $db->query($equiposQuery);
	$ligasQuery = "SELECT * FROM liga";
	$ligasArray = $db->query($ligasQuery);
	$deportesQuery = "SELECT * FROM categorias";
	$deportesArray = $db->query($deportesQuery);


	if(isset($_GET['delete']) && !empty($_GET['delete']))
	{
		$deleteID = $_GET['delete'];
		$cate = mysqli_fetch_assoc($db->query("SELECT * FROM equipo WHERE id='$deleteID'"));
		$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cate['imagen'];
		unlink($imgUrl);
		$db->query("DELETE FROM equipo WHERE id='$deleteID'");
		header('Location: equipos.php');
	}

	$description='';
	$nameC = '';
	$img = '';
	$uploadedPath = '';
	$uploadName = '';
	$dbPath = '';
	$cateE;
	$deporte = '';
	$liga = '';
	$errors = '';

	if(isset($_GET['edit']) && !empty($_GET['edit']))
	{
		$editID = $_GET['edit'];
		$cateE = mysqli_fetch_assoc($db->query("SELECT * FROM equipo WHERE id='$editID'"));
		$lID = mysqli_fetch_assoc($db->query("SELECT * FROM equipo WHERE id='$editID'"))['liga_id'];
		$dID = mysqli_fetch_assoc($db->query("SELECT * FROM liga WHERE id='$lID'"))['cat_id'];
		$nameC = $cateE['nombre'];
		$img = $cateE['imagen'];
		?>
		<script type="text/javascript">
			getLigas(<?= $dID; ?>, <?= $lID; ?>)
		</script>
		<?php
		if(isset($_GET['deleteImage']))
		{
			$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cateE['imagen'];
			unlink($imgUrl);
			$deleteImageQuery = "UPDATE equipo SET imagen='' WHERE id='$editID'";
			$db->query($deleteImageQuery);
			header('Location: equipos.php?edit='.$editID);
		}
	}

	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['liga']) && !empty($_POST['liga']))
		{
			$liga = sanitize($_POST['liga']);
		}

		if(isset($_POST['nombre']) && !empty($_POST['nombre']))
		{
			$nameC = sanitize($_POST['nombre']);
		}

		if(isset($_POST['description']) && !empty($_POST['description']))
		{
			$nameC = sanitize($_POST['description']);
		}

		$photo = $_FILES['photo'];
		if(isset($_FILES) && !empty($_FILES))
		{
			if($photo['size'] > 0)
			{
				$name = $photo['name'];
				$nameArray = explode('.', $name);
				$fileName = $nameArray[0];
				$fileExtension = $nameArray[1];
				$mime = explode('/', $photo['type']);
				$mimeType = $mime[0];
				$mimeExtension = $mime[1];
				$tmpLoc = $photo['tmp_name'];
				$fileSize = $photo['size'];
				$allowed = array('png', 'jpg', 'jpeg', 'gif', 'svg');
				$uploadName = md5(microtime()).'.'.$fileExtension;
				$uploadedPath = BASEURL.'img/equipos/'.$uploadName;
				$dbPath = '/deportesNic/img/equipos/'.$uploadName;



				if($fileSize > 15000000)
				{
					$errors = 'La imagen debe ser de 15mb o menos: '.$fileSize;
				}
				if(!in_array($fileExtension, $allowed))
				{
					$errors = 'La foto debe ser png, jpg, jpeg, gif, svg.';
				}
				if($mimeType != 'image')
				{
					$errors = 'Debes ingresar una imagen';
				}
			}
		}
		if($errors == '')
		{
			if(isset($_GET['edit']))
			{
				if ($dbPath == '')
				{
					$dbPath = $cateE['imagen'];
				}	
				$updateProductQuery = "UPDATE equipo SET nombre='$nameC', imagen='$dbPath', liga_id='$liga' WHERE id='$editID'";
				$db->query($updateProductQuery);
				header('Location: equipos.php');
				if(isset($_FILES) && !empty($_FILES))
				{
					if($photo['size'] > 0)
						move_uploaded_file($tmpLoc, $uploadedPath);
				}
			}
			else
			{
				$insertProductQuery = "INSERT INTO `equipo` (`nombre`, `imagen`, `liga_id`, `descripcion`)
				 VALUES ('$nameC', '$dbPath', '$liga', '$description')";
				$db->query($insertProductQuery);
				move_uploaded_file($tmpLoc, $uploadedPath);
				header('Location: equipos.php');
			}
		}
	}
	
?>

<h2 class="text-center"></h2>

<div class="container-fluid row formSpacing">
	<? if(isset($_GET['add']) && !empty($_GET['add']) || isset($_GET['edit']) && !empty($_GET['edit'])) : ?>
	<div class="col-md-6">
		<form class="form" action="equipos.php<?= ((isset($_GET['edit'])) ? '?edit='.$editID : '?add=-1'); ?>" method="post" enctype="multipart/form-data">
			<legend><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> Equipo</legend>
			<div id="errors"></div>
			<div class="form-group">
				<label for="dep">Seleccionar deporte</label>
				<select id="dep" name="deporte" class="form-control">
					<option></option>
					<? while($dep = mysqli_fetch_assoc($deportesArray)) : ?>
						<option <?= (( isset($_GET['edit']) && $dep['id'] == $dID) ? ' selected ' : '') ?> value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
					<? endwhile; ?>
				</select>

				<label for="lig">Seleccionar liga</label>
				<select id="lig" name="liga" class="form-control">
					<option></option>
				</select>
			</div>
			<div class="form-group">
				<label for="Nombre">Nombre</label>
				<input class="form-control" placeholder="Ingresa el nombre" type="text" value="<?= $nameC; ?>" name="nombre">
			</div>

			<div class="form-group">
				<label for="message">Descripcion</label>
				<textarea rows="3" class="form-control" maxlength="500" placeholder="Descripcion (500 caracteres)" name="description" id="message" ><?=$description; ?></textarea>
			</div>

			<div class="form-group">
				<? if($img != '') : ?>
					<div>
						<img style="max-width: 100px; max-height: 100px" src="<?= $img; ?>">
						<hr>
						<a href="equipos.php?edit=<?= $editID; ?>&deleteImage=1" class="btn btn-danger">Eliminar imagen</a>
					</div>
				<? else : ?>
					<label for="photo">Imagen</label>
					<input type="file" name="photo" id="photo" class="form-control">
				<? endif; ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> Equipo</button>
				<a href="equipos.php" class="btn btn-default">Cancelar</a>
			</div>
			
		</form>
		<? if ($errors != '') : ?>
			<div class="alert alert-danger"><?= $errors ?></div>
		<?endif;?>
	</div>
	<? else: ?>
		<div class="col-md-6 form-group">
				<center><a href="equipos.php?add=1" style="margin-top: 100px;" class="btn btn-success">Añadir equipo</a></center>
			</div>
	<? endif; ?>
	<!--Category table-->
	<div class="col-md-6">
		<div class="form-group">
			<label for="verDep">Seleccionar deporte</label>
			<select id="verDep" class="form-control">
				<option></option>
				<?php
					$depQuery = "SELECT * FROM categorias";
					$depArray = $db->query($depQuery);
				?>
				<? while($dep = mysqli_fetch_assoc($depArray)) : ?>
					<option value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
				<? endwhile; ?>
			</select>

			<label for="verLig">Seleccionar liga</label>
			<select id="verLig" class="form-control">
				<option></option>
			</select>
		</div>
	<h2>Equipos</h2>
		<table class="table table-bordered table-condensed">
			<thead>
				<th class="text-center">Equipos</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Eliminar</th>
			</thead>

			<tbody id="infoEquipos">
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$('select[id="dep"]').change(getLigas);
	$('select[id="verDep"]').change(function () {
		var ID = $('#verDep').val();
		$.ajax({
		url : '/deportesNic/helpers/getLigas.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#verLig').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los ligas :('); }
		});
	});

	$('select[id="verLig"]').change(function () {
		var ID = $('#verLig').val();
		$.ajax({
		url : '/deportesNic/helpers/getEquipos.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#infoEquipos').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los ligas :('); }
		});
	});
</script>