<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    Menu
                </button>
                <a class="navbar-brand" href="../index.php">Deportes Nicaragua</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ver o Añadir<span class="caret"></span></a>
                    <ul class="dropdown-menu sidebar-nav adminDrop" role="menu">
                        <li><a href="categorias.php">Categorias</a></li>
                        <li><a href="ligas.php">Ligas</a></li>
                        <li><a href="equipos.php">Equipos</a></li>
                        <li><a href="eventos.php">Evento</a></li>
                        <!--<li><a href="eventos.php">Tabla de resultados</a></li>-->
                    </ul>
                </li>
                    <li class="dropdown"">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> Cuenta<span class="caret"></span></a>
                    <ul class="dropdown-menu sidebar-nav adminDrop" role="menu">
                        <li><a href="configurarCuenta.php"><span class="glyphicon glyphicon-cog"></span> Configurar cuenta</a></li>
                        <li><a href="logOut.php"><span class="glyphicon glyphicon-off"></span> Salir</a></li>
                    </ul>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>