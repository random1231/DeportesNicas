<?php
$bienvenida = false;
 if(isset($_SESSION['successFlash']) && !empty($_SESSION['successFlash']))
 {
    $bienvenida = true;
 }

 $contra = false;
 if(isset($_SESSION['contra']) && !empty($_SESSION['contra']))
 {
    $contra = true;
 }
?>
<header>
        <div class="container" id="maincontent">
            <div class="row">
            <div style="height: 10px;">
            <center><p class="bien"><?= ($bienvenida == true ? 'Bienvenido '. $firstName : ''); ?></p></center>
            <center><p class="bien"><?= ($contra == true ? 'Tu contraseña ha sido actualizada' : ''); ?></p></center>
            </div>
                <div class="col-lg-12">
                    <div class="intro-text">
                        <h1 class="name">Administracion</h1>
                        <br>
                        <span class="skills">Añade nuevas categorias, eventos, resultados, actualizar informacion, etc..</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <h3>
        <? if($bienvenida == true)
            {
                unset($_SESSION['successFlash']);
            }

            if($contra == true)
            {
                unset($_SESSION['contra']);
            }
        ?>

    </h3>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".bien").fadeOut(5000);
        });
    </script>
