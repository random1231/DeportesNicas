<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';

	if(!isLoggedIn())
	{
		loginErrorRedirect();
	}
	
	include 'includes/head.php';
	include 'includes/navigation.php';

	$equiposQuery = "SELECT * FROM equipo";
	$equiposArray = $db->query($equiposQuery);
	$ligasQuery = "SELECT * FROM liga";
	$ligasArray = $db->query($ligasQuery);
	$deportesQuery = "SELECT * FROM categorias";
	$deportesArray = $db->query($deportesQuery);
	$finalizar = false;
	$finalizarEdit = false;

	if(isset($_GET['finalizar']) && !empty($_GET['finalizar']) && isset($_GET['id']) && !empty($_GET['id']))
	{
		$finalizar = true;
		$editID = $_GET['id'];
		$finEv = mysqli_fetch_assoc($db->query("SELECT * FROM evento  WHERE id='$editID'"));
		$eq1 = $finEv['equipo1'];
		$eq2 = $finEv['equipo2'];
		$equ1Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq1'"))['nombre'];
		$equ2Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq2'"))['nombre'];
	}

	if(isset($_GET['delete']) && !empty($_GET['delete']))
	{
		$deleteID = $_GET['delete'];
		$db->query("DELETE FROM evento WHERE id='$deleteID'");
		header('Location: eventos.php');
	}
	$nameC = '';
	$cateE;
	$deporte = '';
	$liga = '';
	$errors = '';
	$description = '';
	$lugar = '';
	$fecha = '';
	$hora = '';
	$equipo1 = '';
	$equipo2 = '';

	if(isset($_GET['edit']) && !empty($_GET['edit']))
	{
		if(isset($_GET['fin']) && !empty($_GET['fin']))
		{
			$finalizar = true;
			$finalizarEdit = true;
			$editID = $_GET['edit'];
			$finEv = mysqli_fetch_assoc($db->query("SELECT * FROM evento  WHERE id='$editID'"));
			$eq1 = $finEv['equipo1'];
			$eq2 = $finEv['equipo2'];
			$equ1Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq1'"))['nombre'];
			$equ2Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq2'"))['nombre'];
			$res1 = (int)$finEv['resultadoEquipo1'];
			$res2 = (int)$finEv['resultadoEquipo2'];
			$desF = $finEv['descripcion_finalizado'];
		}
		else
		{
			$editID = $_GET['edit'];
			$cateE = mysqli_fetch_assoc($db->query("SELECT * FROM evento WHERE id='$editID'"));
			$lID = mysqli_fetch_assoc($db->query("SELECT * FROM evento WHERE id='$editID'"))['liga_id'];
			$dID = mysqli_fetch_assoc($db->query("SELECT * FROM liga WHERE id='$lID'"))['cat_id'];
			$nameC = $cateE['nombre'];
			$description = $cateE['descripcion'];
			$lugar = $cateE['lugar'];
			$fecha = $cateE['fecha'];
			$hora = $cateE['hora'];
			$equipo1 = $cateE['equipo1'];
			$equipo2 = $cateE['equipo2'];
			?>
			<script type="text/javascript">
				getLigas(<?= $dID; ?>, <?= $lID; ?>);
				getEquipos('#equ1', <?= $lID; ?>, <?= $equipo1; ?>);
				getEquipos('#equ2', <?= $lID; ?>, <?= $equipo2; ?>);
			</script>
		<?php
		}
	}

	if(isset($_POST) && !empty($_POST))
	{

		if(isset($_GET['finEv']) && !empty($_GET['finEv']))
		{
			$editID = sanitize($_GET['finEv']);
			$e1 = sanitize($_POST['equipo1Fin']);
			$e2 = sanitize($_POST['equipo2Fin']);
			$des = '';
			if(isset($_POST['descripcionFin']) && !empty($_POST['descripcionFin']))
			{
				$des = sanitize($_POST['descripcionFin']);
			}

			$updateQuery = "UPDATE evento SET finalizado=1, resultadoEquipo1='$e1', resultadoEquipo2='$e2', descripcion_finalizado='$des' WHERE id='$editID'";
			$db->query($updateQuery);
			header('Location: eventos.php');
		}

		if(isset($_POST['liga']) && !empty($_POST['liga']))
		{
			$liga = sanitize($_POST['liga']);
		}

		if(isset($_POST['nombre']) && !empty($_POST['nombre']))
		{
			$nameC = sanitize($_POST['nombre']);
		}

		if(isset($_POST['descripcion']) && !empty($_POST['descripcion']))
		{
			$description = sanitize($_POST['descripcion']);
		}

		if(isset($_POST['lugar']) && !empty($_POST['lugar']))
		{
			$lugar = sanitize($_POST['lugar']);
		}

		if(isset($_POST['hora']) && !empty($_POST['hora']))
		{
			$hora = sanitize($_POST['hora']);
		}

		if(isset($_POST['fecha']) && !empty($_POST['fecha']))
		{
			$fecha = sanitize($_POST['fecha']);
		}

		if(isset($_POST['equipo1']) && !empty($_POST['equipo1']))
		{
			$equipo1 = sanitize($_POST['equipo1']);
		}

		if(isset($_POST['equipo2']) && !empty($_POST['equipo2']))
		{
			$equipo2 = sanitize($_POST['equipo2']);
		}

		if($equipo1 === $equipo2)
		{
			$errors = 'No puedes seleccionar el mismo equipo dos veces';
		}

		if($errors == '')
		{
			if(isset($_GET['edit']))
			{
				if(isset($_GET['editFin']))
				{
					$editID = sanitize($_GET['editFin']);
					$e1 = sanitize($_POST['equipo1Fin']);
					$e2 = sanitize($_POST['equipo2Fin']);
					$des = '';
					if(isset($_POST['descripcionFin']) && !empty($_POST['descripcionFin']))
					{
						$des = sanitize($_POST['descripcionFin']);
					}

					$updateQuery = "UPDATE evento SET resultadoEquipo1='$e1', resultadoEquipo2='$e2', descripcion_finalizado='$des' WHERE id='$editID'";
					$db->query($updateQuery);
					header('Location: eventos.php');
				}
				else
				{
					$updateQuery = "UPDATE evento SET nombre='$nameC', descripcion='$description', lugar='$lugar', fecha='$fecha', hora='$hora', equipo1='$equipo1', equipo2='$equipo2', liga_id='$liga' WHERE id='$editID'";
					$db->query($updateQuery);
					header('Location: eventos.php');
				}
			}
			else
			{
				$equ1Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$equipo1'"))['nombre'];
				$equ2Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$equipo2'"))['nombre'];
				$insertQuery = "INSERT INTO evento (`nombre`, `descripcion`, `lugar`, `fecha`, `hora`, `equipo1`, `equipo2`, `nombreE1`, `nombreE2`,`liga_id`)
				 VALUES ('$nameC', '$description', '$lugar', '$fecha', '$hora', '$equipo1', '$equipo2', '$equ1Nombre', '$equ2Nombre','$liga')";
				$db->query($insertQuery);
				header('Location: eventos.php');
			}
		}
	}
?>

<div class="container-fluid row formSpacing">
	<!--Form-->
	<? if ($errors != '') : ?>
		<?endif;?>
	<? if(isset($_GET['add']) && !empty($_GET['add']) || isset($_GET['edit']) && !empty($_GET['edit']) && !isset($_GET['fin'])) : ?>
	<div class="col-md-6">
		<?if($errors != '') : ?>
		<div class="alert alert-danger"><?= $errors ?></div>
		<? endif;?>
		<form class="form" action="eventos.php<?= ((isset($_GET['edit'])) ? '?edit='.$editID : '?add=-1'); ?>" method="post" enctype="multipart/form-data">
			<legend><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> Evento</legend>
			<div id="errors"></div>
			<div class="form-group">
				<label for="dep">Seleccionar deporte</label>
				<select id="dep" name="deporte" class="form-control">
					<option></option>
					<? while($dep = mysqli_fetch_assoc($deportesArray)) : ?>
						<option <?= (( isset($_GET['edit']) && $dep['id'] == $dID) ? ' selected ' : '') ?> value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
					<? endwhile; ?>
				</select>

				<label for="lig">Seleccionar liga</label>
				<select id="lig" name="liga" class="form-control">
					<option></option>
				</select>
			</div>

			<div class="form-group row">
				<div class="col-xs-6 col-sm-6 col-md-6">
				<label for="equ1">Equipo 1</label>
				<select id="equ1" name="equipo1" class="form-control">
					<option></option>
				</select>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6">
				<label for="equ2">Equipo 2</label>
				<select id="equ2" name="equipo2" class="form-control">
					<option></option>
				</select>
				</div>
			</div>

			<div class="form-group">
				<label for="Nombre">Nombre</label>
				<input class="form-control" placeholder="Ingresa el nombre" type="text" value="<?= $nameC; ?>" name="nombre">
			</div>

			<div class="form-group">
				<label for="message">Descripcion</label>
				<textarea rows="3" class="form-control" maxlength="500" placeholder="Descripcion (500 caracteres)" name="descripcion" id="message" ><?=$description; ?></textarea>
			</div>

			<div class="form-group">
				<label for="lugar">Lugar</label>
				<input class="form-control" placeholder="Ingrese lugar" name="lugar" id="lugar" value="<?=$lugar; ?>">
			</div>

			<div class="form-group">
				<label for="fecha">Fecha</label>
				<input type="date" class="form-control" name="fecha" id="fecha" value="<?= $fecha ?>">
			</div>

			<div class="form-group">
				<label for="hora">Hora</label>
				<input type="time" class="form-control" name="hora" id="hora" value="<?= $hora ?>">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> Evento</button>
				<a href="eventos.php" class="btn btn-default">Cancelar</a>
			</div>
		</form>
	</div>

	<? elseif($finalizar == true) : ?>
		<div class="col-md-6">
			<h3><?= $finEv['nombre']; ?></h3>
			<?php
				$finID = $finEv['id'];
			?>
			<form class="form" action="eventos.php?<?=((isset($_GET['edit'])) ? 'edit=1&editFin='.$editID : 'finEv='.$finID); ?>" method="post" w enctype="multipart/form-data">
				<div class="form-group row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="equ1fin"><?= $equ1Nombre ?></label>
						<input type="number" min="0" id="equ1fin" name="equipo1Fin" required data-validation-required-message="Ingresa este campo" value="<?= ($finalizarEdit == true) ? $res1 : ''?>"  class="form-control">
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="equ2fin"><?= $equ2Nombre ?></label>
						<input type="number" min="0" id="equ2fin" name="equipo2Fin" required data-validation-required-message="Ingresa este campo" value="<?= ($finalizarEdit == true) ? $res2 : ''?>" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<textarea rows="3" class="form-control" maxlength="500" placeholder="Descripcion (500 caracteres)" name="descripcionFin" id="message" ><?= ($finalizarEdit == true) ? $desF : ''?></textarea>
					</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success"><?= ($finalizarEdit == true) ? 'Editar' : 'Finalizar'?> Evento</button>
					<a href="eventos.php" class="btn btn-default">Cancelar</a>
				</div>
		</div>
		</form>

	<? else: ?>
		<div class="col-md-6 form-group">
				<center><a href="eventos.php?add=1" style="margin-top: 100px;" class="btn btn-success">Añadir Evento</a></center>
			</div>
	<? endif; ?>
	<div class="col-md-6">
		<div class="form-group">
			<label for="verDep">Seleccionar deporte</label>
			<select id="verDep" class="form-control">
				<option></option>
				<?php
					$depQuery = "SELECT * FROM categorias";
					$depArray = $db->query($depQuery);
				?>
				<? while($dep = mysqli_fetch_assoc($depArray)) : ?>
					<option value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
				<? endwhile; ?>
			</select>

			<label for="verLig">Seleccionar liga</label>
			<select id="verLig" class="form-control">
				<option></option>
			</select>
			<br>
			<label for="finalizado">¿Finalizado?</label>
			<input id="finalizado" type="checkbox" name="">
		</div>
	<h2>Eventos</h2>
		<table class="table table-responsive table-bordered table-condensed table-striped">
			<thead>
				<th class="text-center">Equipos</th>
				<th class="text-center">Fecha</th>
				<th class="text-center">Hora</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Eliminar</th>
				<th class="text-center">Finalizar</th>
			</thead>
			<tbody id="infoEv">
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">

	function getTable(finalizado)
	{
		var ID = $('#verLig').val();
		$.ajax({
		url : '/deportesNic/helpers/getEventosTable.php',
		type : 'POST',
		data : {ID : ID, finalizado : finalizado},
		success : function(htmlData, x){
			$('#infoEv').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
		});
	}

	$('select[id="dep"]').change(getLigas);
	$('select[id="verDep"]').change(function () {
		var ID = $('#verDep').val();
		$.ajax({
		url : '/deportesNic/helpers/getLigas.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#verLig').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los eventos :('); }
		});
	});

	$('select[id="verLig"]').change(function () {
		if($('#finalizado').is(':checked'))
			getTable(true);
		else
			getTable(false);
	});

	$('input[type="checkbox"]').click(function() {
		if($('#finalizado').is(':checked'))
			getTable(true);
		else
			getTable(false);
	});

	$('select[id="lig"]').change(function(){
		getEquipos('#equ1', $('#lig').val());
	});

	$('select[id="lig"]').change(function(){
		getEquipos('#equ2', $('#lig').val());
	});


</script>