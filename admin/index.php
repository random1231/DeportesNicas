<?php
    require_once '../core/init.php';

    if(!isLoggedIn())
    {
        loginErrorRedirect();
    }
    include_once 'includes/head.php';
    require_once 'includes/navigation.php';
    require_once 'includes/header.php';
?>
</html>