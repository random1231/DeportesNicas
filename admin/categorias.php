<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';

	if(!isLoggedIn())
	{
		loginErrorRedirect();
	}
	
	include 'includes/head.php';
	include 'includes/navigation.php';

	$categoriesQuery = "SELECT * FROM categorias";
	$categoriesArray = $db->query($categoriesQuery);


	if(isset($_GET['delete']) && !empty($_GET['delete']))
	{
		$deleteID = $_GET['delete'];
		$cate = mysqli_fetch_assoc($db->query("SELECT * FROM categorias WHERE id='$deleteID'"));
		$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cate['imagen'];
		unlink($imgUrl);
		$db->query("DELETE FROM categorias WHERE id='$deleteID'");
		header('Location: categorias.php');
	}
	$nameC = '';
	$description = '';
	$img = '';
	$uploadedPath = '';
	$uploadName = '';
	$dbPath = '';
	$cateE;
	$errors = '';

	if(isset($_GET['edit']) && !empty($_GET['edit']))
	{
		$editID = $_GET['edit'];
		$cateE = mysqli_fetch_assoc($db->query("SELECT * FROM categorias WHERE id='$editID'"));
		$nameC = $cateE['nombre'];
		$description = $cateE['descripcion'];
		$img = $cateE['imagen'];
		if(isset($_GET['deleteImage']))
		{
			$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cateE['imagen'];
			unlink($imgUrl);
			$deleteImageQuery = "UPDATE categorias SET imagen='' WHERE id='$editID'";
			$db->query($deleteImageQuery);
			header('Location: categorias.php?edit='.$editID);
		}
	}

	if(isset($_GET['incluida']) && !empty($_GET['incluida']) && isset($_GET['id']) && !empty($_GET['id']))
	{
		$incID = $_GET['id'];
		$incVal = $_GET['incluida'];
		$db->query("UPDATE categorias SET incluida='$incVal' WHERE id='$incID'");
		header('Location: categorias.php');
	}

	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['nombre']) && !empty($_POST['nombre']))
		{
			$nameC = sanitize($_POST['nombre']);
		}

		if(isset($_POST['descripcion']) && !empty($_POST['descripcion']))
		{
			$description = sanitize($_POST['descripcion']);
		}
		

		$photo = $_FILES['photo'];
		if(isset($_FILES) && !empty($_FILES))
		{
			if($photo['size'] > 0)
			{
				$name = $photo['name'];
				$nameArray = explode('.', $name);
				$fileName = $nameArray[0];
				$fileExtension = $nameArray[1];
				$mime = explode('/', $photo['type']);
				$mimeType = $mime[0];
				$mimeExtension = $mime[1];
				$tmpLoc = $photo['tmp_name'];
				$fileSize = $photo['size'];
				$allowed = array('png', 'jpg', 'jpeg', 'gif', 'svg');
				$uploadName = md5(microtime()).'.'.$fileExtension;
				$uploadedPath = BASEURL.'img/categorias/'.$uploadName;
				$dbPath = '/deportesNic/img/categorias/'.$uploadName;



				if($fileSize > 15000000)
				{
					$errors = 'La imagen debe ser de 15mb o menos: '.$fileSize;
				}
				if(!in_array($fileExtension, $allowed))
				{
					$errors = 'La foto debe ser png, jpg, jpeg, gif, svg.';
				}
				if($mimeType != 'image')
				{
					$errors = 'Debes ingresar una imagen';
				}
			}
		}
		if($errors == '')
		{
			if(isset($_GET['edit']))
			{
				if ($dbPath == '')
				{
					$dbPath = $cateE['imagen'];
				}	
				$updateProductQuery = "UPDATE categorias SET nombre='$nameC', descripcion='$description', imagen='$dbPath' WHERE id='$editID'";
				$db->query($updateProductQuery);
				header('Location: categorias.php');
				if(isset($_FILES) && !empty($_FILES))
				{
					if($photo['size'] > 0)
						move_uploaded_file($tmpLoc, $uploadedPath);
				}
			}
			else
			{
				$insertProductQuery = "INSERT INTO categorias (`nombre`, `descripcion`, `imagen`)
				VALUES('$nameC','$description','$dbPath')";
				$db->query($insertProductQuery);
				move_uploaded_file($tmpLoc, $uploadedPath);
				header('Location: categorias.php');
			}
		}
	}
	
?>

<h2 class="text-center"></h2>

<div class="container-fluid row formSpacing">
	<!--Form-->
	<? if(isset($_GET['add']) && !empty($_GET['add']) || isset($_GET['edit']) && !empty($_GET['edit'])) : ?>
	<div class="col-md-6">
		<form class="form" action="categorias.php<?= ((isset($_GET['edit'])) ? '?edit='.$editID : '?add=-1'); ?>" method="post" enctype="multipart/form-data">
			<legend><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> categoria</legend>
			<div id="errors"></div>
			<div class="form-group">
				<label for="Nombre">Nombre</label>
				<input class="form-control" placeholder="Ingresa el nombre" type="text" value="<?= $nameC; ?>" name="nombre">
			</div>
			<div class="form-group">
				<label for="message">Descripcion</label>
				<textarea rows="5" class="form-control" maxlength="200" placeholder="Descripcion (200 caracteres)" name="descripcion" id="message" required data-validation-required-message="Ingresa la descripcion" ><?=$description; ?></textarea>
			</div>

			<div class="form-group">
				<? if($img != '') : ?>
					<div>
						<img style="max-width: 100px; max-height: 100px" src="<?= $img; ?>">
						<hr>
						<a href="categorias.php?edit=<?= $editID; ?>&deleteImage=1" class="btn btn-danger">Eliminar imagen</a>
					</div>
				<? else : ?>
					<label for="photo">Imagen</label>
					<input type="file" name="photo" id="photo" class="form-control">
				<? endif; ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> categoria</button>
				<a href="categorias.php" class="btn btn-default">Cancelar</a>
			</div>
			
		</form>
		<? if ($errors != '') : ?>
			<div class="alert alert-danger"><?= $errors ?></div>
		<?endif;?>
	</div>
	<? else: ?>
		<div class="col-md-6 form-group">
				<center><a href="categorias.php?add=1" style="margin-top: 100px;" class="btn btn-success">Añadir categoria</a></center>
			</div>
	<? endif; ?>
	<!--Category table-->
	<div class="col-md-6">
	<h2>Categorias</h2>
		<table class="table table-bordered table-condensed">
			<thead>
				<th class="text-center">Categorias</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Eliminar</th>
				<th class="text-center">Incluir/Excluir</th>
			</thead>

			<tbody>
				<!--Parent category-->
				<?php while($parent = mysqli_fetch_assoc($categoriesArray)) :
					$parentID = (int)$parent['id']; //ID of the parent
					$childrenCategoriesQuery = "SELECT * FROM categories WHERE parent='$parentID'";
					$childrenCategoriesQueryResult = $db->query($childrenCategoriesQuery);
				?>
				<tr class="">
					<td class="text-center"><?= $parent['nombre']; ?></td>
					<td align="center">
						<a href="categorias.php?edit=<?= $parentID; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
					</td>
					<td align="center">
						<a href="categorias.php?delete=<?= $parentID; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove-sign"></span></a>
					</td>
					<td align="center">
						<a href="categorias.php?incluida=<?= (($parent['incluida'] == -1) ? '1': '-1'); ?>&id=<?= $parentID; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-<?= (($parent['incluida']) == 1) ? 'minus' : 'plus' ?>"></span></a>
					</td>
				</tr>

			<?php endwhile; ?>
			</tbody>
		</table>
	</div>

</div>

<?php //include 'includes/footer.php';?>