<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';

	if(!isLoggedIn())
	{
		loginErrorRedirect();
	}
	
	include 'includes/head.php';
	include 'includes/navigation.php';

	$ligasQuery = "SELECT * FROM liga";
	$ligasArray = $db->query($ligasQuery);
	$deportesQuery = "SELECT * FROM categorias";
	$deportesArray = $db->query($deportesQuery);


	if(isset($_GET['delete']) && !empty($_GET['delete']))
	{
		$deleteID = $_GET['delete'];
		$cate = mysqli_fetch_assoc($db->query("SELECT * FROM liga WHERE id='$deleteID'"));
		$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cate['imagen'];
		unlink($imgUrl);
		$db->query("DELETE FROM liga WHERE id='$deleteID'");
		header('Location: ligas.php');
	}
	$nameC = '';
	$description = '';
	$img = '';
	$uploadedPath = '';
	$uploadName = '';
	$dbPath = '';
	$cateE;
	$deporte = '';
	$errors = '';

	if(isset($_GET['edit']) && !empty($_GET['edit']))
	{
		$editID = $_GET['edit'];
		$cateE = mysqli_fetch_assoc($db->query("SELECT * FROM liga WHERE id='$editID'"));
		$nameC = $cateE['nombre'];
		$description = $cateE['descripcion'];
		$img = $cateE['imagen'];
		if(isset($_GET['deleteImage']))
		{
			$imgUrl = $_SERVER['DOCUMENT_ROOT'].$cateE['imagen'];
			unlink($imgUrl);
			$deleteImageQuery = "UPDATE liga SET imagen='' WHERE id='$editID'";
			$db->query($deleteImageQuery);
			header('Location: ligas.php?edit='.$editID);
		}
	}

	if(isset($_GET['incluida']) && !empty($_GET['incluida']) && isset($_GET['id']) && !empty($_GET['id']))
	{
		$incID = $_GET['id'];
		$incVal = $_GET['incluida'];
		$db->query("UPDATE liga SET incluida='$incVal' WHERE id='$incID'");
		header('Location: ligas.php');
	}

	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['deporte']) && !empty($_POST['deporte']))
		{
			$deporte = sanitize($_POST['deporte']);
		}

		if(isset($_POST['nombre']) && !empty($_POST['nombre']))
		{
			$nameC = sanitize($_POST['nombre']);
		}

		if(isset($_POST['descripcion']) && !empty($_POST['descripcion']))
		{
			$description = sanitize($_POST['descripcion']);
		}

		$photo = $_FILES['photo'];
		if(isset($_FILES) && !empty($_FILES))
		{
			if($photo['size'] > 0)
			{
				$name = $photo['name'];
				$nameArray = explode('.', $name);
				$fileName = $nameArray[0];
				$fileExtension = $nameArray[1];
				$mime = explode('/', $photo['type']);
				$mimeType = $mime[0];
				$mimeExtension = $mime[1];
				$tmpLoc = $photo['tmp_name'];
				$fileSize = $photo['size'];
				$allowed = array('png', 'jpg', 'jpeg', 'gif', 'svg');
				$uploadName = md5(microtime()).'.'.$fileExtension;
				$uploadedPath = BASEURL.'img/ligas/'.$uploadName;
				$dbPath = '/deportesNic/img/ligas/'.$uploadName;



				if($fileSize > 15000000)
				{
					$errors = 'La imagen debe ser de 15mb o menos: '.$fileSize;
				}
				if(!in_array($fileExtension, $allowed))
				{
					$errors = 'La foto debe ser png, jpg, jpeg, gif, svg.';
				}
				if($mimeType != 'image')
				{
					$errors = 'Debes ingresar una imagen';
				}
			}
		}
		if($errors == '')
		{
			if(isset($_GET['edit']))
			{
				if ($dbPath == '')
				{
					$dbPath = $cateE['imagen'];
				}	
				$updateProductQuery = "UPDATE liga SET nombre='$nameC', descripcion='$description', imagen='$dbPath', cat_id='$deporte' WHERE id='$editID'";
				$db->query($updateProductQuery);
				header('Location: ligas.php');
				if(isset($_FILES) && !empty($_FILES))
				{
					if($photo['size'] > 0)
						move_uploaded_file($tmpLoc, $uploadedPath);
				}
			}
			else
			{
				$insertProductQuery = "INSERT INTO liga (`nombre`, `descripcion`, `imagen`, `cat_id`)
				VALUES('$nameC','$description','$dbPath', '$deporte')";
				
				/*echo $nameC.'<br>';
				echo $description.'<br>';
				echo $dbPath.'<br>';
				echo $db->error;
				assert($db->query($insertProductQuery));
				die();*/
				$db->query($insertProductQuery);
				move_uploaded_file($tmpLoc, $uploadedPath);
				header('Location: ligas.php');
			}
		}
	}
	
?>

<h2 class="text-center"></h2>

<div class="container-fluid row formSpacing">
	<!--Form-->
	<? if(isset($_GET['add']) && !empty($_GET['add']) || isset($_GET['edit']) && !empty($_GET['edit'])) : ?>
	<div class="col-md-6">
		<form class="form" action="ligas.php<?= ((isset($_GET['edit'])) ? '?edit='.$editID : '?add=-1'); ?>" method="post" enctype="multipart/form-data">
			<legend><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> liga</legend>
			<div id="errors"></div>
			<div class="form-group">
				<label for="dep">Seleccionar deporte</label>
				<select id="dep" name="deporte" class="form-control">
					<option></option>
					<? while($dep = mysqli_fetch_assoc($deportesArray)) : ?>
						<option <?= (( isset($_GET['edit']) && $dep['id'] == $cateE['cat_id']) ? ' selected ' : '') ?> value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
					<? endwhile; ?>
				</select>
			</div>
			<div class="form-group">
				<label for="Nombre">Nombre</label>
				<input class="form-control" placeholder="Ingresa el nombre" type="text" value="<?= $nameC; ?>" name="nombre">
			</div>
			<div class="form-group">
				<label for="message">Descripcion</label>
				<textarea rows="5" class="form-control" maxlength="400" placeholder="Descripcion (200 caracteres)" name="descripcion" id="message" required data-validation-required-message="Ingresa la descripcion" ><?=$description; ?></textarea>
			</div>

			<div class="form-group">
				<? if($img != '') : ?>
					<div>
						<img style="max-width: 100px; max-height: 100px" src="<?= $img; ?>">
						<hr>
						<a href="ligas.php?edit=<?= $editID; ?>&deleteImage=1" class="btn btn-danger">Eliminar imagen</a>
					</div>
				<? else : ?>
					<label for="photo">Imagen</label>
					<input type="file" name="photo" id="photo" class="form-control">
				<? endif; ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><?= ((isset($_GET['edit'])) ? 'Editar' : 'Añadir') ?> liga</button>
				<a href="ligas.php" class="btn btn-default">Cancelar</a>
			</div>
			
		</form>
		<? if ($errors != '') : ?>
			<div class="alert alert-danger"><?= $errors ?></div>
		<?endif;?>
	</div>
	<? else: ?>
		<div class="col-md-6 form-group">
				<center><a href="ligas.php?add=1" style="margin-top: 100px;" class="btn btn-success">Añadir liga</a></center>
			</div>
	<? endif; ?>
	<div class="col-md-6">
		<div class="form-group">
			<label for="verDep">Seleccionar deporte</label>
			<select id="verDep" class="form-control">
				<option></option>
				<?php
					$depQuery = "SELECT * FROM categorias";
					$depArray = $db->query($depQuery);
				?>
				<? while($dep = mysqli_fetch_assoc($depArray)) : ?>
					<option value="<?= $dep['id']; ?>"><?= $dep['nombre']; ?></option>
				<? endwhile; ?>
			</select>
		</div>
	<h2>Ligas</h2>
		<table class="table table-bordered table-condensed">
			<thead>
				<th class="text-center">ligas</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Eliminar</th>
			</thead>

			<tbody id="ligasTable">

			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$('select[id="verDep"]').change(function() {
		var ID = $('#verDep').val();
		$.ajax({
		url : '/deportesNic/helpers/getLigasTable.php',
		type : 'POST',
		data : {ID : ID},
		success : function(htmlData, x){
			$('#ligasTable').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los ligas :('); }
		});
	});
</script>