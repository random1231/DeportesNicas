<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';

	if(!isLoggedIn())
	{
		loginErrorRedirect();
	}

	$userID = (int)$_SESSION['sessionUserID'];
	$userData = mysqli_fetch_assoc($db->query("SELECT * FROM usuarios WHERE id='$userID'"));

	$hashed = $userData['contrasena'];

	$oldPassword = '';
	if(isset($_POST['oldPassword']))
	{
		$oldPassword = sanitize($_POST['oldPassword']);
		$oldPassword = trim($oldPassword);
	}

	$password = '';
	if(isset($_POST['password']))
	{
		$password = sanitize($_POST['password']);
		$password = trim($password);
	}

	$confirmPassword = '';
	if(isset($_POST['confirmPassword']))
	{
		$confirmPassword = sanitize($_POST['confirmPassword']);
		$confirmPassword = trim($confirmPassword);
	}

	$newHashed = password_hash($password, PASSWORD_DEFAULT);


	$errors = '';

	if(isset($_POST) && !empty($_POST))
	{
		//Form validation
		if(empty($_POST['oldPassword']) || empty($_POST['password']) || empty($_POST['confirmPassword']))
		{

			$errors = 'Todos los campos son obligatorios';
		}
		//password > 6
		else if(strlen($password) < 6)
		{
			$errors = 'Ingresa una cadena con al menos 6 caracteres';
		}

		else if ($password != $confirmPassword)
		{
			$errors = 'Las contraseñas no son iguales';
		}

		else if(!password_verify($oldPassword, $hashed))
		{
			$errors = "Tu contraseña actual no es la misma";
		}

		else if ($password === $confirmPassword)
		{
			//Change password
			$db->query("UPDATE usuarios SET contrasena='$newHashed' WHERE id='$userID'");
			$_SESSION['contra'] = 'Tu contraseña ha sido actualizada';
			header('Location: index.php');
		}
	}

	include 'includes/head.php';
	include 'includes/navigation.php';

	?>

<div style="margin-top: 110px" class="col-md-6">
<h2>Cambiar Contraseña</h2>
<form action="configurarCuenta.php" method="post">
		<div class="form-group">
			<label for="oldPassword">Contraseña actual</label>
			<input type="password" name="oldPassword" id="oldPassword" class="form-control" value="<?=$oldPassword?>">
		</div>

		<div class="form-group">
			<label for="password">Nueva contraseña</label>
			<input type="password" name="password" id="password" class="form-control" value="<?= $password ?>">
		</div>

		<div class="form-group">
			<label for="confirmPassword">Confirmar nueva contraseña:</label>
			<input type="password" name="confirmPassword" id="confirmPassword" class="form-control" value="<?= $confirmPassword ?>">
		</div>

		<div class="form-group">
			<a href="index.php" class="btn btn-warning">Cancelar</a>
			<input type="submit" class="btn btn-primary" value="Cambiar">
		</div>
	</form>
	<? if($errors != '') : ?>
		<div class="alert alert-danger">
			<?= $errors ?>
		</div>
	<? endif; ?>
</div>