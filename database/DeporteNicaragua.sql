-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 13, 2018 at 07:55 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `DeporteNicaragua`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `incluida` smallint(6) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `imagen`, `incluida`) VALUES
(32, 'Beisbol', 'El beisbol es el deporte madre en Nicaragua con una rica historia y destacados peloteros a nivel mundial, mantente informado acerca del acontecer de la pelota pinolera.', '/deportesNic/img/categorias/847dbbe7bc3050056b40cf04eb491a2f.png', 1),
(35, 'Natacion', 'Natacion asda', '/deportesNic/img/categorias/cfe9e7c05954b622ca1347aee1a288dc.png', 1),
(37, 'Ajedrez', 'asdasdasd', '/deportesNic/img/categorias/2370e5d381cf28d1f464f61319705b8c.png', 1),
(45, 'Pesas', 'hajksdhkajsda', '/deportesNic/img/categorias/f749700a09e3f1b8e1e63fa02ab3b1d7.png', 1),
(48, 'Futbol', 'El futbol es aburrido', '/deportesNic/img/categorias/bf0b58d07ab40ac1b67e4f542755d8d0.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipo`
--

CREATE TABLE `equipo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `liga_id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_esperanto_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;

--
-- Dumping data for table `equipo`
--

INSERT INTO `equipo` (`id`, `nombre`, `imagen`, `liga_id`, `descripcion`) VALUES
(27, 'Los Dantos', '/deportesNic/img/equipos/93bff86149a2566fb228878391b63600.jpg', 33, ''),
(30, 'Boer', '/deportesNic/img/equipos/c6b1a2372213918e86e8c246f62757c1.jpg', 33, ''),
(31, 'Orientales', '/deportesNic/img/equipos/c14bd6fb662a354f03ce31450888417c.jpg', 27, ''),
(32, 'Tigres de Chinandega', '', 27, ''),
(33, 'Los Indios Boer', '', 27, ''),
(34, 'Gigantes de Rivas', '', 27, ''),
(35, 'Orientales', '', 33, ''),
(36, 'Rivas', '', 33, ''),
(37, 'La Costa', '', 33, ''),
(38, 'Real Esteli', '', 29, ''),
(39, 'Walter Ferreti', '', 29, ''),
(41, 'Los Leones son buenos', '/deportesNic/img/equipos/5269fa51e795a2880da399fd925daed7.png', 27, '');

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_finalizado` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lugar` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `equipo1` int(11) NOT NULL,
  `equipo2` int(11) NOT NULL,
  `nombreE1` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nombreE2` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `finalizado` int(11) NOT NULL DEFAULT '0',
  `resultadoEquipo1` int(11) NOT NULL,
  `resultadoEquipo2` int(11) NOT NULL,
  `liga_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `evento`
--

INSERT INTO `evento` (`id`, `nombre`, `descripcion`, `descripcion_finalizado`, `lugar`, `fecha`, `hora`, `equipo1`, `equipo2`, `nombreE1`, `nombreE2`, `finalizado`, `resultadoEquipo1`, `resultadoEquipo2`, `liga_id`) VALUES
(11, 'Los Indios Boer vs Gigantes de rivas', 'Los Indios Boer y Gigantes de rivas se enfrentan.', 'Los indios derrotaron a los Gigantes.', 'Estadio Nacional Denis Martinez', '2017-04-27', '13:00:00', 33, 34, 'Los Indios Boer', 'Gigantes de Rivas', 1, 4, 1, 27),
(12, 'Gigantes de Rivas vs Los Indios del Boer', 'Los indios del Boer se enfrentan a los Gigantes de Rivas en la final de la LBPN, no te lo pierdas. Este partido sera transmitido por canal 13.\r\n', 'Rivas gano', 'Estadio Nacional Denis Martinez', '2017-10-05', '14:00:00', 34, 33, 'Gigantes de Rivas', 'Los Indios Boer', 1, 3, 0, 27),
(13, 'Boer vs Chinadega', 'El Boer enfrenta a los tigres de Chinandega quienes tienen una racha de 4 partidos sin perder. &iquest;Podra el Boer vencerlos? No te pierdas este partido. asoidopais', NULL, 'Estadio Nacional Denis Martinez', '2017-10-01', '13:00:00', 33, 32, 'Los Indios Boer', 'Tigres de Chinandega', 0, 0, 0, 27),
(14, 'Orientales vs Chinandega (Final de la LBPN)', 'Los Orientales de Granada de enfrentan al Chinandega en la el primer partido de la final de la LBPN.', 'Los Orientales se consagran campeones por primera vez en la LBPN tras haber derrotado al Chinandega en un partido muy emocionante.', 'Estadio Roque Tadeo Zavala', '2017-11-01', '16:00:00', 31, 32, 'Orientales', 'Tigres de Chinandega', 1, 3, 2, 27),
(15, 'Prueba', 'asdas', NULL, 'sadas', '2017-02-02', '13:00:00', 34, 32, 'Gigantes de Rivas', 'Tigres de Chinandega', 0, 0, 0, 27);

-- --------------------------------------------------------

--
-- Table structure for table `liga`
--

CREATE TABLE `liga` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `descripcion` varchar(400) COLLATE utf8_esperanto_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;

--
-- Dumping data for table `liga`
--

INSERT INTO `liga` (`id`, `nombre`, `descripcion`, `imagen`, `cat_id`) VALUES
(27, 'Liga Profesional de Beisbol', 'La liga de profesional de beisbol, con 17 a&ntilde;os de existencia la LBPN, es la mas importante en la pelota pinolera. Esta edicion de la liga arranca el 10 de noviembre del 2017 y acaba el 30 de febrero 2018. Los partidos seran transmitidos por canal 13. &iexcl;No te lo pierdas!', '/deportesNic/img/ligas/f12fbae31059ca1196a0fbaa92508998.png', 32),
(29, 'Liga Nacional de futbol', 'asdadas', '/deportesNic/img/ligas/64f24285b52594001ce3d609f8e6d9b7.png', 48),
(33, 'Liga German Pomares', 'aasdas', '/deportesNic/img/ligas/7da4889aa4b9f3d5425efa7f97948dfc.png', 32);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `email` varchar(175) COLLATE utf8_esperanto_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_esperanto_ci NOT NULL,
  `permisos` varchar(255) COLLATE utf8_esperanto_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `contrasena`, `permisos`) VALUES
(1, 'Hans Blanco', 'hansblancodev@gmail.com', '$2y$10$5TtSQ2Rj4jK2nt5DQ.gWleFD5rN0vMxTaqRl5VBLIwXbAWpgtUyyi', 'admin,editor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipo_ibfk_1` (`liga_id`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `liga_id` (`liga_id`),
  ADD KEY `equipo1` (`equipo1`),
  ADD KEY `equipo2` (`equipo2`);

--
-- Indexes for table `liga`
--
ALTER TABLE `liga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `liga_ibfk_1` (`cat_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `equipo`
--
ALTER TABLE `equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `liga`
--
ALTER TABLE `liga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`liga_id`) REFERENCES `liga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`liga_id`) REFERENCES `liga` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `evento_ibfk_2` FOREIGN KEY (`equipo1`) REFERENCES `equipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evento_ibfk_3` FOREIGN KEY (`equipo2`) REFERENCES `equipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `liga`
--
ALTER TABLE `liga`
  ADD CONSTRAINT `liga_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
