<?php
	require_once "head.php";
	require_once "../helpers/helpers.php";

	$id = '';
	$finalizado = 0;
	if ( isset($_GET['id']) && !empty($_GET['id']) )
	{
		$id = $_GET['id'];
	}

	if ( isset($_GET['finalizado']) && !empty($_GET['finalizado']) )
	{
		$finalizado = $_GET['finalizado'];
	}

	$getEvQuery = "select ev.* from evento ev where ev.liga_id in (select l.id from liga l where l.cat_id = '$id') and ev.finalizado='$finalizado'";

	$getEv = $db->query($getEvQuery);

	$depCount = mysqli_num_rows($getEv);

	$data = '';
	$x = 1; //This variable is used to add or to not add the coma in the json string

	if($depCount < 1)
	{
		echo 'Error: No hay deportes en la base de datos';
	}

	else
	{
		while( $ev = mysqli_fetch_assoc($getEv) ) //Reading the content in the database
		{
			//Building the json string
			$data .= '{';
			$data .= '"id":"'.$ev['id'].'", ';
			$data .= '"nombre":"'.$ev['nombre'].'", ';
			$data .= '"descripcion":"'.html_entity_decode($ev['descripcion']).'", ';
			$data .= '"descripcion_finalizado":"'.html_entity_decode($ev['descripcion_finalizado']).'", ';
			$data .= '"lugar":"'.$ev['lugar'].'", ';
			$data .= '"fecha":"'.fecha($ev['fecha']).'", ';
			$data .= '"hora":"'.hora($ev['hora']).'", ';
			$data .= '"nombreE1":"'.$ev['nombreE1'].'", ';
			$data .= '"nombreE2":"'.$ev['nombreE2'].'", ';
			$data .= '"resultadoEquipo1":"'.$ev['resultadoEquipo1'].'", ';
			$data .= '"resultadoEquipo2":"'.$ev['resultadoEquipo2'].'" ';
			$data .= '}';

			if($x < $depCount ) //Add coma?
				$data .= ',';
			++$x;

	}
		echo "[{$data}]";
}
?>