<?php
	require_once "head.php";

	$ID = '';
	if ( isset($_GET['id']) && !empty($_GET['id']) )
	{
		$ID = $_GET['id'];
	}

	$getligaQuery = "SELECT * FROM liga WHERE id='$ID'";
	$getliga = $db->query($getligaQuery);

	$depCount = mysqli_num_rows($getliga); //Number of sports in the database

	$data = '';//This is the data we are gonna send in json
	$x = 1; //This variable is used to add or to not add the coma in the json string

	/*
		Checking if there are sports in the database.
		If there are not, that means that there is an error in the query...
		Or maybe the table is actually empty.
	*/
	if($depCount < 1)
	{
		echo 'Error: No hay deportes en la base de datos';
	}

	else
	{
		while( $liga = mysqli_fetch_assoc($getliga) ) //Reading the content in the database
		{
			//Building the json string
			$data .= '{';
			$data .= '"nombre":"'.$liga['nombre'].'", ';
			$data .= '"descripcion":"'.html_entity_decode($liga['descripcion']).'", ';
			$data .= '"imagen":"'.$liga['imagen'].'", ';
			$data .= '}';

			if($x < $depCount ) //Add coma?
				$data .= ',';
			++$x;

		echo "[{$data}]";
	}
}

?>