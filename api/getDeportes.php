<?php
	require_once "head.php";

	$getDeportesQuery = "SELECT * FROM categorias";
	$getDeportes = $db->query($getDeportesQuery);

	$depCount = mysqli_num_rows($getDeportes); //Number of sports in the database

	$data = '';//This is the data we are gonna send in json
	$x = 1; //This variable is used to add or to not add the coma in the json string

	/*
		Checking if there are sports in the database.
		If there are not, that means that there is an error in the query...
		Or maybe the table is actually empty.
	*/
	if($depCount < 1)
	{
		echo 'Error: No hay deportes en la base de datos';
	}

	else
	{
		while( $deporte = mysqli_fetch_assoc($getDeportes) ) //Reading the content in the database
		{
			//Building the json string
			$data .= '{';
			$data .= '"id":"'.$deporte['id'].'", ';
			$data .= '"nombre":"'.$deporte['nombre'].'", ';
			$data .= '"descripcion":"'.html_entity_decode($deporte['descripcion']).'", ';
			$data .= '"imagen":"'.$deporte['imagen'].'"';
			$data .= '}';

			if($x < $depCount ) //Add coma?
				$data .= ',';
			++$x;
		}

		echo "[{$data}]";
	}

?>