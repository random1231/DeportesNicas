<?php
	//This is used in equipos.php for retrieving information about ligas.
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	$ID = (int)$_POST['ID'];
	$ligaID = '';
	if(isset($_POST['lID']))
		$ligaID = (int)$_POST['lID'];
	$ligas = $db->query("SELECT * FROM liga WHERE cat_id='$ID' ORDER BY nombre");
	ob_start();
?>
<option value=""></option>
<? while($lig = mysqli_fetch_assoc($ligas)) : ?>
	<option <?= ( ( $ligaID != '' && $ligaID == $lig['id']) ? ' selected ' : '') ?> value="<?= $lig['id']; ?>"><?= $lig['nombre']; ?></option>
<? endwhile; ?>
<?= ob_get_clean(); ?>