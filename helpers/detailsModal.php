<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/helpers/helpers.php';
    $id = (int)$_POST['id'];
    $detailsQuery = "SELECT * FROM evento WHERE id='$id'";
    $res = mysqli_fetch_assoc($db->query($detailsQuery));
    $finalizado = (int)$res['finalizado'];
    $eq1 = $res['equipo1'];
    $eq2 = $res['equipo2'];
    $equ1Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq1'"))['nombre'];
    $equ2Nombre = mysqli_fetch_assoc($db->query("SELECT * FROM equipo  WHERE id='$eq2'"))['nombre'];
    $e1Res = $res['resultadoEquipo1'];
    $e2Res = $res['resultadoEquipo2'];

?>

<div class="portfolio-modal modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h1><?= $res['nombre']; ?></h1>
                        <hr>
                        <p class="text-justified"><?= (($finalizado == 0 ? $res['descripcion'] : $res['descripcion_finalizado'])); ?></p>
                        <br>
                        <? if($finalizado == 1) : ?>
                            <div class="col-md-6">
                            <h3><?= $equ1Nombre; ?></h3>
                            <h3><?= $e1Res ?></h3>
                            </div>
                            <div class="col-md-6">
                            <h3><?= $equ2Nombre; ?></h3>
                            <h3><?= $e2Res; ?></h3>
                            </div>
                        <?else : ?>
                        <p><strong>Fecha:</strong> <?= fecha($res['fecha']); ?></p>
                        <p><strong>Hora:</strong> <?= hora($res['hora']); ?></p>
                        <p><strong>Lugar:</strong> <?= $res['lugar']; ?></p>
                        </ul>
                        <? endif; ?>
                        <button style="margin-top: 20px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('body').on('hidden.bs.modal', '.modal', function () {
        $('.modal').remove();
    });
</script>