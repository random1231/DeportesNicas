<?php
	//This is used in ligas.php for retrieving information about ligas in the table.
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/helpers/helpers.php';
	$ID = (int)$_POST['ID'];
	$finalizado = $_POST['finalizado'];
	$evQuery = '';
	if($finalizado == 'true')
		$evQuery = "SELECT * FROM evento WHERE liga_id='$ID' AND finalizado = 1 ORDER BY fecha DESC";
	else
		$evQuery = "SELECT * FROM evento WHERE liga_id='$ID' AND finalizado = 0 ORDER BY fecha DESC";
	$evResult = $db->query($evQuery);
?>

<?php while($parent = mysqli_fetch_assoc($evResult)) : ?>
<tr class="">
	<td class="text-center"><?= $parent['nombre']; ?></td>
	<td class="text-center"><?= fecha($parent['fecha']); ?></td>
	<td class="text-center"><?= hora($parent['hora']); ?></td>
	<td align="center">
	<? if($finalizado != 'true') : ?>
		<a href="eventos.php?edit=<?= $parent['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
	<? else : ?>
		<a href="eventos.php?edit=<?= $parent['id']; ?>&fin=1" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
	<? endif; ?>
	</td>
	<td align="center">
		<a href="eventos.php?delete=<?= $parent['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove-sign"></span></a>
	</td>
	<td align="center">
		<? if($finalizado != 'true') : ?>
		<a href="eventos.php?finalizar=<?= (($parent['finalizado'] == 0) ? '1': '-1'); ?>&id=<?= $parent['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-minus-sign"></span></a>
		<? else : ?>
			Finalizado
		<? endif; ?>
	</td>
</tr>

<?php endwhile; ?>