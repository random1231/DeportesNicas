<?php
	//This is used in equipos.php for retrieving information about ligas.
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	$ID = (int)$_POST['ID'];
	$selectID = '';
	if(isset($_POST['selectID']))
		$selectID = (int)$_POST['selectID'];
	$equipos = $db->query("SELECT * FROM equipo WHERE liga_id='$ID' ORDER BY nombre");
	ob_start();
?>
<option value=""></option>
<? while($equi = mysqli_fetch_assoc($equipos)) : ?>
	<option <?= ( ( $selectID != '' && $selectID == $equi['id']) ? ' selected ' : '') ?> value="<?= $equi['id']; ?>"><?= $equi['nombre']; ?></option>
<? endwhile; ?>
<?= ob_get_clean(); ?>