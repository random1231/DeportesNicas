function getLigas(ID, lID)
{
	if(isNaN(ID))
		var ID = $('#dep').val();
	$.ajax({
		url : '/deportesNic/helpers/getLigas.php',
		type : 'POST',
		data : {ID : ID, lID : lID},
		success : function(htmlData, x){
			console.log(htmlData);
			$('#lig').html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los ligas :('); }

	});
}

function getEquipos(equipoDiv, ID, equipo)
{
	$.ajax({
		url : '/deportesNic/helpers/getEquiposEvent.php',
		type : 'POST',
		data : {ID : ID, selectID : equipo},
		success : function(htmlData, x){
			console.log(htmlData);
			$(equipoDiv).html(htmlData);
		},
		error : function(){ alert('Algo anda mal. No se pueden cargar los equipos :('); }
	});
}

function detailsModal(id)
{
	var data = {'id' : id};
	$.ajax({
		url: '/deportesNic/helpers/detailsModal.php',
		method : 'post',
		data : data,
		
		success : function(data) {
			$('body').append(data);
			$('#detailsModal').modal('toggle');
		},

		error : function() {
			alert('No se pueden cargar los detalles :(');
		}
	});
}