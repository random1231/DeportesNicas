<?php
	//This is used in ligas.php for retrieving information about ligas in the table.
	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/core/init.php';
	$ID = (int)$_POST['ID'];
	$ligasQuery = "SELECT * FROM liga WHERE cat_id='$ID'";
	$ligasQueryResult = $db->query($ligasQuery);
?>

<?php while($parent = mysqli_fetch_assoc($ligasQueryResult)) : ?>
<tr class="">
	<td class="text-center"><?= $parent['nombre']; ?></td>
	<td align="center">
		<a href="ligas.php?edit=<?= $parent['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
	</td>
	<td align="center">
		<a href="ligas.php?delete=<?= $parent['id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove-sign"></span></a>
	</td>
</tr>

<?php endwhile; ?>