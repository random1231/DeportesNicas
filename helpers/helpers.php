<?php
	function displayErrors($error)
	{
		$display = '<p class="text-danger">'.$error.'</p>';
		return $display;
	}

	function sanitize($dirty)
	{
		return htmlentities($dirty, ENT_QUOTES, "UTF-8");
	}

	function login($userID)
	{
		$_SESSION['sessionUserID'] = $userID;
		$_SESSION['successFlash'] = 'true';
		header('Location: admin/index.php');
	}

	function isLoggedIn()
	{
		if(isset($_SESSION['sessionUserID']) && $_SESSION['sessionUserID'] > 0)
		{
			return true;
		}

		return false;
	}

	function loginErrorRedirect($url = '../login.php')
	{
		$_SESSION['errorFlash'] = "Debes registrate primero para ingresar a este sitio";
		header('Location: '.$url);
	}

	function permissionErrorRedirect($url = '../login.php')
	{
		$_SESSION['errorFlash'] = "No tienes permisos para entrar a este sitio";
		header('Location: '.$url);
	}

	function hasPermission($permission = 'admin')
	{
		global $userData;
		$permissions = explode(',', $userData['permissions']);
		if(in_array($permission, $permissions, true))
			return true;
		return false;
	}

	function fecha($f)
	{
		$meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		$m = explode('-', $f);
		$ano = $m[0];
		$mes = ((int)$m[1])-1;
		$dia = $m[2];
		$cad = $dia.' de '.$meses[$mes].' del '.$ano;
		return $cad;
	}

	function hora($h)
	{
		$ampm = 'AM';
		$tmp = explode(':', $h);
		if((int)$tmp[0] > 12)
			$ampm = 'PM';
		$hour = (int)$tmp[0]%12;
		if($hour == 0)
			$hour = 12;
		$cad = $hour.':'.$tmp['1'].' '.$ampm;
		return $cad;
	}