<?php
	$dbName = 'DeporteNicaragua';
	$dbHost = 'localhost';
	$dbUsername = 'root';
	$dbUserPassword = '';
	$userData = null;

	$db = mysqli_connect($dbHost, $dbUsername, $dbUserPassword,$dbName);
	$firstName = '';

	if(mysqli_connect_errno())
	{
		echo "No se pudo establecer la conexion a la base de datos debido a:  " . mysqli_connect_error();
		die();
	}
	session_start();


	require_once $_SERVER['DOCUMENT_ROOT'].'/deportesNic/config.php';
	require_once BASEURL.'helpers/helpers.php';

	if (isset($_SESSION['sessionUserID']) && !empty(isset($_SESSION['sessionUserID'])))
	{
		$userID = $_SESSION['sessionUserID'];
		$userQuery = $db->query("SELECT * FROM usuarios WHERE id='$userID'");
		$userData = mysqli_fetch_assoc($userQuery);
		//$fullName = explode(' ', $userData['fullName']);
		$firstName = explode(' ', $userData['nombre'])[0];
		//unset($_SESSION['sessionUserID']);
	}	

	if (isset($_SESSION['errorFlash']))
	{
		echo '<div class="bg-danger"><p class="text-danger text-center">'.$_SESSION['errorFlash'].'</p></div>';
		unset($_SESSION['errorFlash']);
	}
?>