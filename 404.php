<?php
require_once 'includes/head.php';
?>
<body id="page-top" class="index">
<?php
?>

<header>
        <div class="container" id="maincontent" style="padding-top: 20px; height: 100%" tabindex="-1">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-text"><h2 class="name">Error 404 not found</h2></div>
                    <br>
                    <img class="img-responsive" src="img/red-card.png"  style="max-width: 300px; max-height: 300px;" alt="">
                    <div class="intro-text">
                        <br>
                        <h2>¡Oops! Esta pagina no se encuentra</h2>
                        <br>
                        <br>
                        <a href="index.php" class="btn btn-default">Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </header>